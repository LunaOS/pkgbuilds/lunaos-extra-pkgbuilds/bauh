pkgname=bauh
pkgver=0.10.7
pkgrel=3
pkgdesc="Graphical interface for managing your applications (AppImage, Flatpak, Snap, Arch/AUR, Web)"
arch=('any')
url="https://github.com/vinifmor/bauh"
license=('zlib/libpng')
depends=('python' 'python-pyqt5' 'python-pyqt5-sip' 'python-requests' 'python-colorama' 'python-pyaml' 'qt5-svg' 'python-dateutil')
optdepends=('flatpak: required for Flatpak support'
            'python-beautifulsoup4: for Native Web applications support'
            'python-lxml: for Native Web applications support'
            'sqlite3: required for AppImage support'
            'fuse2: may be required for AppImage support'
            'fuse3: may be required for AppImage support'
            'pacman: required for AUR support'
            'binutils: required for AUR support'
            'git: required for AUR support'
            'rebuild-detector: enables rebuild checking for AUR packages (optional)'
            'autoconf: may be required to compile some AUR packages'
            'automake: may be required to compile some AUR packages'
            'bison: may be required to compile some AUR packages'
            'fakeroot: may be required to compile some AUR packages'
            'flex: may be required to compile some AUR packages'
            'gcc: may be required to compile some AUR packages'
            'm4: may be required to compile some AUR packages'
            'lib32-fakeroot: may be required to compile some AUR packages'
            'make: may be required to compile some AUR packages'
            'patch: may be required to compile some AUR packages'
            'pkgconf: may be required to compile some AUR packages'
            'ccache: can improve AUR packages compilation speed'
            'aria2: multi-threaded downloading support'
            'breeze: KDE Plasma main theme'
            'axel: multi-threaded downloading support'
            'shadow: to install AUR packages as the root user'
            'util-linux: to install AUR packages as the root user')
makedepends=('python-build' 'python-installer' 'python-wheel' 'python-setuptools')
_commit=b1ea479a3aed504e0ddc329f2474e9a93331927c
source=("git+https://github.com/vinifmor/bauh.git#commit=$_commit"
        "custom-config.patch"
        "python3.13.patch"
        "gnome-dock.patch")

sha512sums=('a5a5c0ffe25a08094f253a51b63b235e370c21d6ce8ee044c9dbdd2c7ef2ed4eb069c0fc3274de8974f339fdec027e79b0bc1264ead6944de24c516ece4a5f6c'
            'bcff2a95e5862ce4f26bae1d7ea2249890b96cd57591cb4f32d1e4aa8810550df46aee0a1f19f44735b4bea20a63456d4323ebe48ed13da5bb4e3c8c03972273'
            'c575e2a9053cc4115153036cafc5a7897d16672336645bae2fb21318b00fde429a6804fa4af9cb7857e93a664ace63cc3b0117c87cb591e92be6a316882d5fc9'
            'eeb91b29e1a767c6ae87c67ee88692b11ae68abe90f37654f07bad796a4ef290ddc01a78cb4ce5de93dff5ea8ced7ed0601df955befac9f075eb295367365fc7')
        
prepare() {
  patch -d "${srcdir}/${pkgname}" -p1 -i "${srcdir}"/custom-config.patch
  patch -d "${srcdir}/${pkgname}" -p1 -i "${srcdir}"/python3.13.patch
  patch -d "${srcdir}/${pkgname}" -p1 -i "${srcdir}"/gnome-dock.patch
}

build() {
  cd "${srcdir}/${pkgname}"

  # removing outdated setup files
  rm setup.cfg setup.py requirements.txt

  python -m build --wheel --no-isolation
}

package() {
  cd "${srcdir}/${pkgname}"
  python -m installer --destdir="$pkgdir" dist/*.whl

  mkdir -p $pkgdir/usr/share/icons/hicolor/scalable/apps

  cp bauh/view/resources/img/logo.svg $pkgdir/usr/share/icons/hicolor/scalable/apps/bauh.svg

  mkdir -p $pkgdir/usr/share/applications
  mv bauh/desktop/bauh.desktop $pkgdir/usr/share/applications/
  mv bauh/desktop/bauh_tray.desktop $pkgdir/usr/share/applications/

  mkdir -p $pkgdir/etc/bauh/
  echo "debian" > $pkgdir/etc/bauh/gems.forbidden
}
